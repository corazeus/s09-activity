package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
//Will require all routes within the application to use "/greeting" as parts of its routes
//The "@RestController" Annotation tells spring boot that this application will function as an endpoint that will be used in handling web requests

public class DiscussionApplication {

	ArrayList<String> enrollees = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}
/*
	@GetMapping("/hello")
	//Maps a get request to the route "/hello" and the method "hello()"
	public String hello(){
		return "Hello World";
	}

	@GetMapping("/hi")
	//Maps a get request to the route "/hi" and the method "hi()" and requests a parameter for the name.
	//http://localhost:8080/hi?name=Jack
	//%s specifies that the accepted data type of the value is of ANY data type
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Bert") String name, @RequestParam(value = "friend", defaultValue = "Joe") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	//Routes with path variables
	//@PathVariable annotation allows us to extract data directly from the URL.
	@GetMapping("/hello/{name}")
	public String hello (@PathVariable("name") String name){
		return String.format("Nice to meet you %s!", name);
	}*/

	//TODO: ACTIVITY STARTS HERE

	//Activity is to create an enroll type API thats it ily goodluck mwah <3
	//Includes: Enroll new Student, Courses, and Name/Age

	//Enroll
	@PostMapping("/enroll")
	public String enroll(@RequestParam(value = "name", defaultValue = "Jack") String name){
		enrollees.add(name);
		return "Welcome "+name+"!";
	}

	//Get Enrollees
	@GetMapping("/getEnrollees")
	public String getEnrollees(){
		return String.valueOf(enrollees);
	}

	//New Name and Age
	@PostMapping("/nameage")
	public String nameage(@RequestParam(value = "name", defaultValue = "Jack") String name, @RequestParam(value = "age", defaultValue = "69") String age){
		return "Hello "+name+"! My age is "+age;
	}

	//Courses using course id
	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id") String id){

		if(id.equals("java101")){

			return "Name: "+id+", Schedule: MWF 8:00 AM, Price PHP 3000";

		} else if (id.equals("sql101")) {

			return "Name: "+id+", Schedule: WTF 16:09 AM, Price PHP 6069";

		} else if (id.equals("javaee101")) {

			return "Name: "+id+", Schedule: TTH 8:30 AM, Price PHP 2000";

		}else {

			return "Course cannot be found.";

		}
	}
}
